<?php

use Illuminate\Database\Seeder;

use App\Entities\Taxonomy;
use App\Entities\FormElementType;
use App\Entities\Account;

class AccountsSeeder extends Seeder {

    private function seed_form_element_types() {
        Taxonomy::add( 'Form element type', 'Form element types' );

        $form_element_types = [
            'checkbox' => 'Checkbox',
            'flip-switch' => 'FlipSwitch',
            'geo-point' => 'GeoPoint',
            'multiple-select' => 'MultipleSelect',
            'number' => 'Number',
            'select' => 'Select',
            'text' => 'Text',
            'photo' => 'Photo',
            'textarea' => 'Textarea',
            'date' => 'Date',
            'time' => 'Time'
        ];

        foreach( $form_element_types as $name => $blueprint )
            FormElementType::add( $name, $blueprint );
    }

    private function seed_accounts() {
        Taxonomy::add( 'Account', 'Accounts' );

        $account_details = [
            [
                'name' => 'Metajua',
                'form_element_types' => [
                    'checkbox', 'flip-switch', 'geo-point', 'multiple-select',
                    'number', 'select', 'text', 'photo', 'textarea', 'date', 'time'
                ]
            ],
            [
                'name' => 'Naseco',
                'form_element_types' => [
                    'number', 'select', 'text'
                ]
            ]
        ];

        foreach( $account_details as $account_detail )
            Account::add( $account_detail[ 'name' ], null, ( isset( $account_detail[ 'form_element_types' ] ) ? $account_detail[ 'form_element_types' ] : [ 'text', 'number' ] ) );
    }

    public function run() {
        $this->seed_form_element_types();
        $this->seed_accounts();
    }
}
