 <?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

use App\Entities\Account;
use App\Entities\User;

class UsersSeeder extends Seeder {

    public function run() {
        $metajua = new Account( 'Metajua' );
        $naseco = new Account( 'Naseco' );

		$entries = [
            [
                'username' => 'isaac.n@metajua.com',
                'password' => Hash::make( 'isaac' ),
                'meta_data' => [
                    'first_name' => 'Isaac',
                    'last_name' => 'King',
	                'account' => $metajua->id,
                    'has_mobile_app_access' => true,
                    'mobile_app_password' => 'isaac'
                ]
            ],
            [
                'username' => 'william.b@metajua.com',
                'password' => Hash::make( 'william' ),
                'meta_data' => [
                    'first_name' => 'William',
                    'last_name' => 'Bamulanzeki',
                    'account' => $naseco->id,
                    'has_mobile_app_access' => true,
                    'mobile_app_password' => 'william'
                ]
            ],
            [
                'username' => 'afrod.m@metajua.com',
                'password' => Hash::make( 'afrod' ),
                'meta_data' => [
                    'first_name' => 'Afrod',
                    'last_name' => 'Mwine',
                    'account' => $metajua->id,
                    'has_mobile_app_access' => false,
                ]
            ]
        ];

        foreach( $entries as $entry )
            User::add( $entry );
    }

}
