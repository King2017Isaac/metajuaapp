<?php

namespace App\Blueprints;

class Photo extends Blueprint {

    public $attributes = [ 'label', 'required', 'allow_null', 'file_ext', 'file_size', 'min', 'max', 'hidden', 'width', 'height', 'source'  ];
    public $required = [ 'label' ];
    public $defaults = [
        'required' => false,
        'file_ext' => 'jpg',
        'min' => 0,
        'max' => 1,
        'source' => 'camera',
        'file_size' => 2000,
        'hidden' => false,
        'width' => '',
        'height' => '',
        'allow_null' => true,
    ];

    public static function get_defaults() {
        $blueprint = new Photo();

        return $blueprint->defaults;
    }

}