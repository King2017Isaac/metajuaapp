<?php

namespace App\Blueprints;

class Select extends Blueprint {

    public $attributes = [ 'label', 'placeholder', 'required', 'options', 'default', 'allow_null', 'dynamic_options', 'hidden'  ];
    public $required = [ 'label' ];
    public $defaults = [
        'dynamic_options' => false,
        'options' => [],
        'query' => [],
        'required' => false,
        'hidden' => false
    ];

    public static function get_defaults() {
        $blueprint = new Select();

        return $blueprint->defaults;
    }

}