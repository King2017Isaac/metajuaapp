<?php

namespace App\Blueprints;

class Number extends Blueprint{

    public $attributes = [ 'label', 'placeholder', 'value', 'required', 'default', 'allow_null', 'min', 'max', 'hidden'  ];
    public $required = [ 'label' ];
    public $defaults = [
        'required' => false,
        'hidden' => false,
        'min' => 0,
        'max' => 1000
    ];

    public static function get_defaults() {
        $blueprint = new Number();

        return $blueprint->defaults;
    }

}