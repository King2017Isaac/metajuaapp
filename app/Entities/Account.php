<?php

namespace App\Entities;

class Account {

	public $id;
	public $name;

	public function __construct( $identifier ) {
		$term = new Term( 'Account', $identifier );

		if ( $term->id != null ) :

			$this->id = $term->id;
			$this->name = $term->name;

		endif;
	}

	public static function add( $name = '', $user_id = null, $form_element_types = [] ) {
		if ( empty( $name ) )
			return null;

		$term_id = Term::add( 'Account', $name, $user_id );

		TermMeta::add( $term_id, 'form_element_types', $form_element_types );

		return $term_id;
	}

}