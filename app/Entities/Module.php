<?php

namespace App\Entities;

use Illuminate\Support\Facades\DB;

class Module {

	public $id;
	public $name;
	public $meta = [];

	public function __construct( $identifier ) {
		$term = new Term( 'Module', $identifier );

		if ( $term->id != null ) :

			$this->id = $term->id;
			$this->name = $term->name;

			foreach( [ 'account', 'icon', 'order' ] as $meta_key ) :

			    $meta = new TermMeta( $this->id, $meta_key );

			    if ( $meta->id != null )
			        $this->meta[ $meta_key ] = $meta->meta_value;

			endforeach;

		endif;
	}

	public static function add( $name = '', $user_id = null ) {
		if ( empty( $name ) )
			return null;

		return Term::add( 'Module', $name, $user_id );
	}

    public static function get_modules( $accounts = [], $per_page = 0, $page = 0 ) {
	    $taxonomy = new Taxonomy( 'Module' );

	    if ( $taxonomy->id != null ) :

            if ( empty( $accounts ) && $per_page == 0 && $page == 0 ) :

                $records = DB::select( "SELECT id FROM terms WHERE taxonomy_id = :taxonomy_id ORDER BY name ASC", [
                    'taxonomy_id' => $taxonomy->id
                ] );

            else :

                $page = $per_page > 0 && $page == 0 ? 1 : $page;
                $per_page = $page > 0 && $per_page == 0 ? 15 : $per_page;

                if ( !empty( $accounts ) && $per_page == 0 && $page == 0 ) :

                    $records = DB::select(
                        "SELECT terms.id AS id FROM terms LEFT JOIN term_meta ON terms.id = term_meta.term_id WHERE terms.taxonomy_id = :taxonomy_id AND term_meta.meta_key = :account AND term_meta.meta_value IN ( " . implode( ', ', $accounts ) . " ) ORDER BY terms.name ASC", [
                            'taxonomy_id' => $taxonomy->id,
                            'account' => 'account'
                        ]
                    );

                elseif ( empty( $accounts ) && $per_page > 0 ) :

                    $records = DB::select(
                        "SELECT id FROM terms WHERE taxonomy_id = :taxonomy_id ORDER BY name ASC LIMIT :lower_limit, :per_page", [
                            'taxonomy_id' => $taxonomy->id,
                            'lower_limit' => ( $page - 1 ) * $per_page,
                            'per_page' => $per_page
                        ]
                    );

                else :

                    $records = DB::select(
                        "SELECT terms.id AS id FROM terms LEFT JOIN term_meta ON terms.id = term_meta.term_id WHERE terms.taxonomy_id = :taxonomy_id AND term_meta.meta_key = :account AND term_meta.meta_value IN ( " . implode( ', ', $accounts ) . " ) ORDER BY terms.name ASC LIMIT :lower_limit, :per_page", [
                            'taxonomy_id' => $taxonomy->id,
                            'account' => 'account',
                            'lower_limit' => ( $page - 1 ) * $per_page,
                            'per_page' => $per_page
                        ]
                    );

                endif;

            endif;

            foreach( $records as $record )
                $modules[] = new Module( $record->id );

        endif;

        return isset( $modules ) ? $modules : [];
    }

}