<?php

namespace App\Http\Controllers\FormSections;

use App\Entities\FormSection;
use Illuminate\Support\Facades\Input;

use App\Entities\FormElement;
use App\Entities\PostType;

use App\Http\Controllers\Controller;

class FormSectionController extends Controller {

    public function single( $section ) {
        $form_section = new FormSection( $section );

        return response()->json( [
            'result' => $form_section->id == null ? 'failed' : 'successful',
            'module' => $form_section
        ] );
    }

    public function elements( $section ) {
        $form_section = new FormSection( $section );

        if ( $form_section->id == null )
            return response()->json( [
                'result' => 'failed',
                'message' => 'Invalid form section passed'
            ] );

        $post_type = new PostType( 'Form element' );

        if ( $post_type->id == null )
            return response()->json( [
                'result' => 'failed',
                'message' => 'Error occured. Contact administrator'
            ] );

        $per_page = ( int ) Input::get( 'per_page', 15 );
        $page = ( int ) Input::get( 'page', 1 );

        return response()->json( [
            'result' => 'successful',
            'post_type' => $post_type,
            'section_elements' => FormElement::get_form_elements( [ $form_section->id ], $per_page, $page )
        ] );
    }

}