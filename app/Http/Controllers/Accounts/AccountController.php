<?php

namespace App\Http\Controllers\Accounts;

use Illuminate\Support\Facades\Input;

use App\Entities\FormElementType;
use App\Entities\Taxonomy;
use App\Entities\TermMeta;
use App\Entities\Account;
use App\Entities\User;
use App\Entities\Module;

use App\Http\Controllers\Controller;

class AccountController extends Controller {

    public function single( $account ) {
        $account = new Account( $account );

        return response()->json( [
            'result' => $account->id == null ? 'failed' : 'successful',
            'account_id' => $account->id
        ] );
    }

    public function form_element_types( $account ) {
        $account = new Account( $account );

        if ( $account->id == null )
            return response()->json( [
                'result' => 'failed',
                'message' => 'Invalid account passed'
            ] );

        $taxonomy = new Taxonomy( 'Form element type' );

        if ( $taxonomy->id == null )
            return response()->json( [
                'result' => 'failed',
                'message' => 'Error occured. Contact administrator'
            ] );

        $meta = new TermMeta( $account->id, 'form_element_types' );

        $type_names = $meta->meta_value;

        foreach( is_array( $type_names ) ? $type_names : [] as $type_name ) :

            $form_element_type = new FormElementType( $type_name );

            if ( $form_element_type->id != null )
                $form_element_types[] = [
                    'id' => $form_element_type->id,
                    'name' => $form_element_type->name,
                    'blueprint' => $form_element_type->blueprint
                ];

        endforeach;

        return response()->json( [
            'result' => 'successful',
            'taxonomy' => [
                'id' => $taxonomy->id,
                'name' => $taxonomy->name
            ],
            'form_element_types' => isset( $form_element_types ) ? $form_element_types : []
        ] );
    }

    public function users( $account ) {
        $account = new Account( $account );

        if ( $account->id == null )
            return response()->json( [
                'result' => 'failed',
                'message' => 'Invalid account passed'
            ] );

        $per_page = ( int ) Input::get( 'per_page', 15 );
        $page = ( int ) Input::get( 'page', 1 );

        return response()->json( [
            'result' => 'successful',
            'users' => User::get_users( [ $account->id ], $per_page, $page )
        ] );
    }

    public function modules( $account ) {
        $account = new Account( $account );

        if ( $account->id == null )
            return response()->json( [
                'result' => 'failed',
                'message' => 'Invalid account passed'
            ] );

        $taxonomy = new Taxonomy( 'Module' );

        if ( $taxonomy->id == null )
            return response()->json( [
                'result' => 'failed',
                'message' => 'Error occured. Contact administrator'
            ] );

        $per_page = ( int ) Input::get( 'per_page', 15 );
        $page = ( int ) Input::get( 'page', 1 );

        return response()->json( [
            'result' => 'successful',
            'taxonomy' => $taxonomy,
            'modules' => Module::get_modules( [ $account->id ], $per_page, $page )
        ] );
    }

}