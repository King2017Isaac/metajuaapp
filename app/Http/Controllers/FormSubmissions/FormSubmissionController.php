<?php

namespace App\Http\Controllers\FormSubmissions;

use App\Entities\FormElement;
use Illuminate\Support\Facades\Input;

use App\Entities\PostType;
use App\Entities\FormSubmission;
use App\Entities\FormElementResponse;

use App\Http\Controllers\Controller;

class FormSubmissionController extends Controller {

    public function single( $section ) {
        $form_submission = new FormSubmission( $section );

        return response()->json( [
            'result' => $form_submission->id == null ? 'failed' : 'successful',
            'module' => $form_submission
        ] );
    }

    public function responses( $submission ) {
        $form_submission = new FormSubmission( $submission );

        if ( $form_submission->id == null )
            return response()->json( [
                'result' => 'failed',
                'message' => 'Invalid form submission passed'
            ] );

        $post_type = new PostType( 'Form element response' );

        if ( $post_type->id == null )
            return response()->json( [
                'result' => 'failed',
                'message' => 'Error occured. Contact administrator'
            ] );

        $per_page = ( int ) Input::get( 'per_page', 15 );
        $page = ( int ) Input::get( 'page', 1 );

        return response()->json( [
            'result' => 'successful',
            'post_type' => $post_type,
            'responses' => FormElementResponse::get_form_element_responses( [ $form_submission->id ], $per_page, $page )
        ] );
    }

    public function add_response( $submission ) {
        $form_submission = new FormSubmission( $submission );

        if ( $form_submission->id == null )
            return response()->json( [
                'result' => 'failed',
                'message' => 'Invalid form submission passed'
            ] );

        $form_element = new FormElement( ( int ) Input::get( 'form_element', 0 ) );

        if ( $form_element->id == null )
            return response()->json( [
                'result' => 'failed',
                'message' => 'Invalid form element passed'
            ] );

        $user = Input::get( 'user', null );
        $response = Input::get( 'response', '' );

        $current_responses = FormElementResponse::get_form_element_responses( [ $form_submission->id ] );

        if ( count( $current_responses ) > 0 ) :

            $recent = $current_responses[ count( $current_responses ) - 1 ];

            $chunks = explode( '/', $recent->name );

            $next = ( ( ( int ) end( $chunks ) ) + 1 );

        else :

            $next = 1;

        endif;

        $submission_chunks = explode( '/', $form_submission->name );

        $name = 'RSP/' . end( $submission_chunks ) . '/' . str_pad( $next, 6, '0', STR_PAD_LEFT  );

        $form_element_response = new FormElementResponse( FormElementResponse::add( $form_submission->id, $name, $form_element->id, $response, $user ) );

        if ( $form_element_response->id == null )
            return response()->json( [
                'result' => 'failed',
                'message' => 'Failed to add form element response'
            ] );

        return response()->json( [
            'result' => 'successful',
            'data' => [
                'id' => $form_element_response->id,
                'name' => $form_element_response->name
            ]
        ] );
    }

}