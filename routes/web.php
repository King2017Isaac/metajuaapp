<?php

$app->group( [ 'prefix' => '', 'middleware' => 'APICors' ], function () use ( $app ) {
    $app->group( [ 'prefix' => 'account/{account}' ], function () use ( $app ) {
        $app->post( '', [
            'uses' => 'Accounts\AccountController@single'
        ] );

        $app->post( 'form-element-types', [
            'uses' => 'Accounts\AccountController@form_element_types'
        ] );

        $app->post( 'users', [
            'uses' => 'Accounts\AccountController@users'
        ] );

        $app->post( 'modules', [
            'uses' => 'Accounts\AccountController@modules'
        ] );
    } );

    $app->group( [ 'prefix' => 'module/{module}' ], function () use ( $app ) {
        $app->post( '', [
            'uses' => 'Modules\ModuleController@single'
        ] );

        $app->post( 'forms', [
            'uses' => 'Modules\ModuleController@forms'
        ] );
    } );

    $app->group( [ 'prefix' => 'form/{form}' ], function () use ( $app ) {
        $app->post( '', [
            'uses' => 'Forms\FormController@single'
        ] );

        $app->post( 'sections', [
            'uses' => 'Forms\FormController@sections'
        ] );

        $app->group( [ 'prefix' => 'submissions' ], function () use ( $app ) {
            $app->post( '', [
                'uses' => 'Forms\FormController@submissions'
            ] );

            $app->post( 'add', [
                'uses' => 'Forms\FormController@add_submission'
            ] );
        } );
    } );

    $app->group( [ 'prefix' => 'form-section/{section}' ], function () use ( $app ) {
        $app->post( '', [
            'uses' => 'FormSections\FormSectionController@single'
        ] );

        $app->post( 'elements', [
            'uses' => 'FormSections\FormSectionController@elements'
        ] );
    } );

    $app->group( [ 'prefix' => 'form-submission/{submission}' ], function () use ( $app ) {
        $app->post( '', [
            'uses' => 'FormSubmissions\FormSubmissionController@single'
        ] );

        $app->group( [ 'prefix' => 'responses' ], function () use ( $app ) {
            $app->post( '', [
                'uses' => 'FormSubmissions\FormSubmissionController@responses'
            ] );

            $app->post( 'add', [
                'uses' => 'FormSubmissions\FormSubmissionController@add_response'
            ] );
        } );
    } );
} );